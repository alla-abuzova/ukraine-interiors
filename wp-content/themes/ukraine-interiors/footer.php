        <footer>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-sm">
                        <div id="sidebar-copyright" class="sidebar">
                            <?php dynamic_sidebar( 'copyright' ); ?>
                        </div>git
                    </div>
                    <div class="col-sm"></div>
                    <div class="col-sm">
                        <div id="sidebar-information" class="sidebar">
                            <?php dynamic_sidebar( 'information' ); ?>
                        </div>
                        <!--<ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">Active</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">Disabled</a>
                            </li>
                        </ul>-->
                    </div>
                    <div class="col-sm">
                        <div id="sidebar-social-icons" class="sidebar">
                            <?php dynamic_sidebar( 'social-icons' ); ?>
                        </div>
                        <!--<ul class="nav flex-column">
                            <li class="nav-item">
                                <a class="nav-link active" href="#">Active</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Link</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">Disabled</a>
                            </li>
                        </ul>-->
                    </div>
                </div>

            </div>
        </footer>
    </body>
</html>