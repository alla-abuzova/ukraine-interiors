<?php

// Adding styles and scripts files
add_action( 'wp_enqueue_scripts', 'ukraine_interiors_scripts' );

function ukraine_interiors_scripts() {
    wp_enqueue_style( 'theme-style', get_stylesheet_uri() );
    wp_enqueue_style( 'style1', get_theme_file_uri('/assets/dist/css/bootstrap.css'));
    wp_enqueue_style( 'style2', get_theme_file_uri('/assets/dist/css/bootstrap-grid.css'));
    wp_enqueue_style( 'style3', get_theme_file_uri('/assets/dist/css/bootstrap-reboot.css'));
    wp_enqueue_script( 'script3', "https://code.jquery.com/jquery-3.3.1.slim.min.js");
    wp_enqueue_script( 'script2', "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js");
    wp_enqueue_script( 'script1', "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js");
    wp_enqueue_script( 'script4', get_theme_file_uri('/assets/dist/js/bootstrap.js'));
    wp_enqueue_script( 'script5', get_theme_file_uri('/assets/dist/js/bootstrap.bundle.js'));
}

// Register sitebars

add_action( 'widgets_init', 'my_register_sidebars' );
function my_register_sidebars() {

    register_sidebar(
        array(
            'id'            => 'social-icons',
            'name'          => __( 'Social icons' ),
            'description'   => __( 'Sidebar for social icons menu' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );

    register_sidebar(
        array(
            'id'            => 'information',
            'name'          => __( 'Information' ),
            'description'   => __( 'Sidebar for information menu' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );

    register_sidebar(
        array(
            'id'            => 'copyright',
            'name'          => __( 'Copyright' ),
            'description'   => __( 'Sidebar for copyright block' ),
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<h3 class="widget-title">',
            'after_title'   => '</h3>',
        )
    );
}

// Register menu

function register_header_menus() {
    register_nav_menus(
        array(
            'header-menu' => __( 'Header Menu' ),
            'extra-menu' => __( 'Extra Menu' )
        )
    );
}
add_action( 'init', 'register_header_menus' );


add_action( 'init', 'register_kitchens' );
function register_kitchens(){
    register_post_type( 'kitchens', [
        'label'  => null,
        'labels' => [
            'name'               => 'Kitchens', // основное название для типа записи
            'singular_name'      => '____', // название для одной записи этого типа
            'add_new'            => 'Добавить Kitchens', // для добавления новой записи
            'add_new_item'       => 'Добавление ____', // заголовка у вновь создаваемой записи в админ-панели.
            'edit_item'          => 'Редактирование ____', // для редактирования типа записи
            'new_item'           => 'Новое ____', // текст новой записи
            'view_item'          => 'Смотреть ____', // для просмотра записи этого типа.
            'search_items'       => 'Искать ____', // для поиска по этим типам записи
            'not_found'          => 'Не найдено', // если в результате поиска ничего не было найдено
            'not_found_in_trash' => 'Не найдено в корзине', // если не было найдено в корзине
            'parent_item_colon'  => '', // для родителей (у древовидных типов)
            'menu_name'          => 'Kitchens', // название меню
        ],
        'description'         => '',
        'public'              => true,
        // 'publicly_queryable'  => null, // зависит от public
        // 'exclude_from_search' => null, // зависит от public
        // 'show_ui'             => null, // зависит от public
        // 'show_in_nav_menus'   => null, // зависит от public
        'show_in_menu'        => null, // показывать ли в меню адмнки
        // 'show_in_admin_bar'   => null, // зависит от show_in_menu
        'show_in_rest'        => null, // добавить в REST API. C WP 4.7
        'rest_base'           => null, // $post_type. C WP 4.7
        'menu_position'       => null,
        'menu_icon'           => null,
        //'capability_type'   => 'post',
        //'capabilities'      => 'post', // массив дополнительных прав для этого типа записи
        //'map_meta_cap'      => null, // Ставим true чтобы включить дефолтный обработчик специальных прав
        'hierarchical'        => false,
        'supports'            => [ 'title', 'editor' ], // 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
        'taxonomies'          => [],
        'has_archive'         => false,
        'rewrite'             => true,
        'query_var'           => true,
    ] );
}


function wporg_add_custom_box() {
    $screens = [ 'kitchens', 'wporg_cpt' ];
    foreach ( $screens as $screen ) {
        add_meta_box(
            'wporg_box_id',                 // Unique ID
            'Custom Meta Box Title',      // Box title
            'wporg_custom_box_html',  // Content callback, must be of type callable
            $screen                            // Post type
        );
    }
}
add_action( 'add_meta_boxes', 'wporg_add_custom_box' );

/*function wporg_custom_box_html( $post ) {
    ?>
    <label for="wporg_field">Description for this field</label>
    <select name="wporg_field" id="wporg_field" class="postbox">
        <option value="">Select something...</option>
        <option value="something">Something</option>
        <option value="else">Else</option>
    </select>
    <?php
}*/

function wporg_custom_box_html( $post ) {
    $value = get_post_meta( $post->ID, '_wporg_meta_key', true );
    ?>
    <label for="wporg_field">Description for this field</label>
    <select name="wporg_field" id="wporg_field" class="postbox">
        <option value="">Select something...</option>
        <option value="something" <?php selected( $value, 'something' ); ?>>Something</option>
        <option value="else" <?php selected( $value, 'else' ); ?>>Else</option>
    </select>
    <?php
}

function wporg_save_postdata( $post_id ) {
    if ( array_key_exists( 'wporg_field', $_POST ) ) {
        update_post_meta(
            $post_id,
            '_wporg_meta_key',
            $_POST['wporg_field']
        );
    }
}
add_action( 'save_post', 'wporg_save_postdata' );


function register_room_type()
{
    register_taxonomy('room_type', 'kitchens', $args= array('label'));
}
add_action( 'init', 'register_room_type' );

function kategorii_dlja_stranic(){
    register_taxonomy_for_object_type( 'category', 'page');
}

add_action( 'init', 'kategorii_dlja_stranic' );