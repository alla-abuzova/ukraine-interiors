<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php wp_head(); ?>
    </head>
    <body>
    <header>
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Ukraine Interiors</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="#">ТРЕНДЫ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">ДИЗАЙН</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">АРХИТЕКТУРА</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">АРТ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">ИНТЕРЬЕРЫ</a>
                        </li>
                    </ul>
                    <form class="form-inline my-2 my-lg-0">
                        <input class="form-control mr-sm-2" type="search" placeholder="Search">
                        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                    </form>
                </div>
            </nav>
        </div>
    </header>









